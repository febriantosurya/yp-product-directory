const express = require('express');
const query = require('./query')

const app = express();
const PORT = 3000;

app.get('/category', query.getCategory);
app.get('/all-product', query.getAllProduct);
app.get('/product-by-category', query.getProductByCategory);
app.get('/product-detail/:id', query.getDetailProduct);

app.listen(PORT, () => {
  console.log(`Running on PORT : ${PORT}`);
});