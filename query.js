const { Sequelize } = require('sequelize');

//connection
const sequelize = new Sequelize('postgres', 'febri', '', {
    host: 'localhost',
    dialect: 'postgres'
  }
);

sequelize.authenticate().then(() => {
  console.log('Database Connected.');
}).catch((error) => {
  console.error('Database connection error: ', error);
});

var category = sequelize.define('categories', {
  id : {
    type: Sequelize.BIGINT,
    primaryKey: true
  },
  name : Sequelize.TEXT
}, {
  timestamps: false,
  createdAt: false,
  updatedAt: false,
});

var product = sequelize.define('products', {
  id : {
    type: Sequelize.BIGINT,
    primaryKey: true
  },
  categoryId : Sequelize.BIGINT,
  name : Sequelize.TEXT,
  stock : Sequelize.BIGINT
}, {
  timestamps: false,
  createdAt: false,
  updatedAt: false,
});

category.hasMany(product);
product.belongsTo(category);

exports.getCategory = (req, res) => {
  sequelize.sync().then(() => {
    category.findAll().then(result => {
      res.send(result);
    }).catch((error) => {
      console.error('Failed to retrieve data : ', error);
    });
  });
};

exports.getAllProduct = (req, res) => {
  sequelize.sync().then(() => {
    product.findAll().then(result => {
      res.send((result));
    }).catch((error) => {
      console.error('Failed to retrieve data : ', error);
    });
  });
};

exports.getProductByCategory = (req, res) => {
  sequelize.sync().then(() => {
    product.findAll({
      where: {categoryId:1},
      include: { model: category, required: false },
    }).then(result => {
        res.send(result);
    }).catch((error) => {
        console.error('Failed to retrieve data : ', error);
    });
  });
};

exports.getDetailProduct = (req, res) => {
  sequelize.sync().then(() => {
    product.findByPk(req.params.id).then(result => {
        res.send(result);
    }).catch((error) => {
        console.error('Failed to retrieve data : ', error);
    });
  });
}